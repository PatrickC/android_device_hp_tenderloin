LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

ifeq ($(TARGET_PREBUILT_KERNEL),)
TARGET_PREBUILT_KERNEL := device/hp/tenderloin/prebuilt/boot/kernel
endif

# include the non-open-source counterpart to this file
-include vendor/hp/tenderloin/AndroidBoardVendor.mk
